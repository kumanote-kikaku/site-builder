(function() {
  'use strict';

  /* eslint no-console: 0 */

  var node_fs = require('fs');
  var node_path = require('path');

  var curdir = node_path.resolve('.');
  var libdir = node_path.dirname(__filename);
  var tmpldir = node_path.join(libdir, 'template');
  var basedir = node_path.dirname(node_path.dirname(libdir));

  function logPath(path, prefix) {
    if (!prefix) {
      prefix = '';
    }
    console.log('  ' + prefix + node_path.relative(basedir, path));
  }

  function mkdirp(path) {
    var stat;

    if (node_path.dirname(path) === path) {
      return;
    }
    mkdirp(node_path.dirname(path));
    try {
      stat = node_fs.statSync(path);
      return;
    } catch (er) {
      if (er.code !== 'ENOENT') {
        return;
      }
    }
    node_fs.mkdirSync(path);
    logPath(path, '[create] ');
  }

  function copyFile(from, dirname, filename) {
    var src = node_path.resolve(node_path.join(tmpldir, dirname, from));
    var dstDir = node_path.resolve(node_path.join(basedir, dirname));
    var dst = node_path.resolve(node_path.join(dstDir, filename));
    var stat;

    mkdirp(dstDir);
    try {
      stat = node_fs.statSync(dst);
      logPath(dst, '[skip] ');
      return;
    } catch (er) {
    }
    node_fs.writeFileSync(dst, node_fs.readFileSync(src));
    logPath(dst, '[create] ');
  }

  function updatePackage() {
    var src = node_path.join(tmpldir, 'package.json');
    var dst = node_path.join(basedir, 'package.json');
    var srcJSON = JSON.parse(node_fs.readFileSync(src, 'UTF-8'));
    var dstJSON = JSON.parse(node_fs.readFileSync(dst, 'UTF-8'));
    var name;
    if (!('devDependencies' in dstJSON)) {
      dstJSON.devDependencies = {};
    }
    for (name in srcJSON.devDependencies) {
      dstJSON.devDependencies[name] = srcJSON.devDependencies[name];
    }
    node_fs.writeFileSync(dst, JSON.stringify(dstJSON, null, 2), 'UTF-8');
    logPath(dst, '[update] ');
  }

  if (curdir !== basedir) {
    throw new Error('setup.js must be run in the base directory');
  }

  console.log('');
  console.log('[Base Directory]');
  console.log(basedir);
  console.log('');

  console.log('[Template Files]');
  copyFile('dot.csscomb.json', '', '.csscomb.json');
  copyFile('dot.gitignore', '', '.gitignore');
  copyFile('dot.editorconfig', '', '.editorconfig');
  copyFile('dot.eslintrc', '', '.eslintrc');
  copyFile('gulpfile.js', '', 'gulpfile.js');
  copyFile('build-config.js', '', 'build-config.js');
  copyFile('php.ini', 'misc', 'php.ini');
  copyFile('php.win32.ini', 'misc', 'php.win32.ini');
  copyFile('php.darwin.ini', 'misc', 'php.darwin.ini');
  copyFile('router-for-wordpress.php', 'misc', 'router-for-wordpress.php');
  copyFile('sprite-template.sass', 'misc', 'sprite-template.sass');

  copyFile('_clearfix.scss', 'scss-example', '_clearfix.scss');
  copyFile('_debug.scss', 'scss-example', '_debug.scss');
  copyFile('_designed.scss', 'scss-example', '_designed.scss');
  copyFile('_font.scss', 'scss-example', '_font.scss');
  copyFile('_lib.scss', 'scss-example', '_lib.scss');
  copyFile('_reset.scss', 'scss-example', '_reset.scss');

  console.log('');

  console.log('[package.json]');
  updatePackage();

  console.log('');
  console.log('[Finish]');
  console.log('# please run \'npm install && gulp check\'');
})();
