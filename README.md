# site-builder

定型的なサイト構築用のビルドスクリプト

## 導入方法

    # 作業ディレクトリ作成＋移動
    mkdir your-project
    cd your-project

    # package.jsonひな形作成
    echo '{}' > package.json

    # 本ライブラリのインストール
    npm -D install https://bitbucket.org/kumanote-kikaku/site-builder.git

    # 本ライブラリを使ったセットアップ
    node node_modules/site-builder/setup.js

    # 依存ライブラリのインストール
    npm install

    # 動作テスト
    gulp check


- `setup.js` がやること
  - `build-config.js` のひな形作成
  - `gulpfile.js` の作成
  - `.csscomb.json` の作成
  - `.editorconfig` の作成
  - `package.json` への `devDependencies` 追加
  - `misc` のコピー

## ディレクトリ構成

### Source
### Cache
### Preview
### Release

## 主なタスク

### gulp clean

- `build` タスクで `Preview` および `Release` に生成されたファイルを削除する
- 以下のディレクトリで、空になるディレクトリを削除する
  - `Cache`
  - `Preview`
  - `Release`

### gulp cache

- `Source` に含まれる画像を最適化し、 `Cache` に格納する
- `Source` に含まれるCSSスプライト画像を元に
  スプライトシートとsassファイルを生成し、 `Cache` に格納する

### gulp build

- `Source` に含まれるファイルをビルドし、 `Preview` と `Release` に格納する
- `Cache` に含まれる画像を、 `Preview` と `Release` に格納する

### watch

- `Source` および `Cache` を監視し、更新があったら
  `build` タスクや `cache` タスクを実行する
- タスクが実行された場合に、 `livereload` が有効であれば実行する

### serve

- ローカルのHTTPサーバを起動する
  - 設定により `gulp-webserver` か `php` を選択可能
  - `Wordpress` の場合に向けたルーティングスクリプトも内包

## 設定ファイル

- ベースディレクトリの直下に生成される `build-config.js` が設定ファイルとなる
- 詳細は `site-builder` の中にある `build-config.sample.js` を参照
