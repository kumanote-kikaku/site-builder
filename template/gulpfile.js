(function() {
  'use strict';

  var gulp = require('gulp');

  var util = require('./node_modules/site-builder/lib/util.js');
  var tasks = util.makeTasks();

  gulp.task('clean-files', function(cb) {
    tasks.clean(cb);
  });

  gulp.task('clean', ['clean-files'], function(cb) {
    util.cleanDirs(cb);
  });

  gulp.task('cache', ['clean'], function(cb) {
    tasks.cache(cb);
  });

  gulp.task('build', ['cache'], function(cb) {
    tasks.build(cb);
  });

  gulp.task('watch', ['build'], function(cb) {
    util.startLivereload();
    tasks.watch(cb);
  });

  gulp.task('default', [util.getDefaultTaskName()]);

  gulp.task('check', function(cb) {
    util.check(cb);
  });
})();
