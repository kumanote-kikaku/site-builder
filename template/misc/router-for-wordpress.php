<?php
// Wordpress用のrouting

if (preg_match('/\.(?:png|jpg|jpeg|gif|css|js)$/', parse_url($_SERVER['REQUEST_URI'])['path'])) {
	// 画像などはそのまま処理
	return false;
}

$DS = DIRECTORY_SEPARATOR;
$filename = $_SERVER['SCRIPT_FILENAME'];
if (!file_exists($filename)) {
	$filename = dirname(dirname(__FILE__)) . $DS . 'Sites' . $DS . 'preview' . $DS . 'index.php';
}
chdir(dirname($filename));
include $filename;
