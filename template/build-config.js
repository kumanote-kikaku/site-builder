var config = {};

// ============================================================================
// gobal configuration

// see .editorconfig !

// default encoding
config.encoding = 'UTF-8';

// default eol character
config.eol = '\x0d\x0a';

// use livereload
config.livereload = true;
// config.livereload = 12345; // set TCP/IP port number
// config.livereload = false; // never use

// ============================================================================
// directory configuration

config.directory = {};

// source directory
config.directory.source = 'Sites/src';

// cache directory
config.directory.cache = 'Sites/cache';

// preview directory
config.directory.preview = 'Sites/preview';

// release directory
config.directory.release = 'Sites/release';

// ============================================================================
// local task configuration

config.task = {};

// ----------------------------------------------------------------------------
// copy

config.task.copy = {};

// source
config.task.copy.source = [];
config.task.copy.source.push('**/*.css');
config.task.copy.source.push('**/*.min.js');
config.task.copy.source.push('**/*.ico');
config.task.copy.source.push('**/*.pdf');
config.task.copy.source.push('**/*.csv');
config.task.copy.source.push('**/*.{xls,xlsx,doc,docx,ppt,pptx}');

// ----------------------------------------------------------------------------
// file convert

config.task.file = {};

// config.task.file.encoding = 'UTF-8'; // task specified encoding
// config.task.file.eol = '\x0d\x0a'; // task specified eol character

// source
config.task.file.source = [];
config.task.file.source.push('**/*.{html,php}');

// library file
config.task.file.library = [];
config.task.file.library.push('**/_*.{html,php}');

// options
config.task.file.options = {};

// include prefix and suffix (for RegExp)
// default
//   @@include('FILE PATH')
//   @@include('FILE PATH', {"key": "value"})
//   @@key
config.task.file.options.prefix = '@@';
config.task.file.options.suffix = '';

// ES2015 style
//   ${include('FILE PATH')}
//   ${include('FILE PATH', {"key": "value"})}
//   $key}
// config.task.file.options.prefix = '\\$\\{';
// config.task.file.options.suffix = '\\}';

// input indent rule
config.task.file.options.input_indent = {};
config.task.file.options.input_indent.character = ' '; // indent character
config.task.file.options.input_indent.size = 2; // character per 1 indent

// output indent rule
config.task.file.options.output_indent = {};
config.task.file.options.output_indent.character = '\t'; // indent character
config.task.file.options.output_indent.size = 1; // character per 1 indent

// ----------------------------------------------------------------------------
// Sass/Scss

config.task.sass = {};

// config.task.sass.encoding = 'UTF-8'; // task specified encoding
// config.task.sass.eol = '\x0d\x0a'; // task specified eol character

// source
config.task.sass.source = [];
config.task.sass.source.push('**/*.{sass,scss}');

// library
config.task.sass.library = [];
config.task.sass.library.push('**/_*.{sass,scss}');


// options

config.task.sass.options = {};

// minify
config.task.sass.options.minify = false;
// config.task.sass.options.minify = true; // minify css file
// config.task.sass.options.minify = 'both'; // minify css file to *.min.css

// autoprefixer
config.task.sass.options.browsers = [];
config.task.sass.options.browsers.push('> 5%');
config.task.sass.options.browsers.push('last 2 versions');
config.task.sass.options.browsers.push('Android 2.2');
config.task.sass.options.browsers.push('iOS 4');
config.task.sass.options.browsers.push('ie 9');
config.task.sass.options.browsers.push('Safari 5');

// ----------------------------------------------------------------------------
// JavaScript

config.task.js = {};

// config.task.js.encoding = 'UTF-8'; // task specified encoding
// config.task.js.eol = '\x0d\x0a'; // task specified eol character

// source
config.task.js.source = [];
config.task.js.source.push('**/*.js');

// options
config.task.js.options = {};

// strip block comment
config.task.js.options.strip = true;

// output indent
config.task.js.options.output_indent = {};
config.task.js.options.output_indent.character = ' '; // indent character
config.task.js.options.output_indent.size = 2; // character per 1 indent

// minify
config.task.js.options.minify = false;
// config.task.js.options.minify = true; // minify js file
// config.task.js.options.minify = 'both'; // minify js file to *.min.js

// ----------------------------------------------------------------------------
// image

config.task.image = {};

// source
config.task.image.source = [];
config.task.image.source.push('**/*.png');
config.task.image.source.push('**/*.gif');
config.task.image.source.push('**/*.svg');
config.task.image.source.push('**/*.{jpeg,jpg}');

// options
config.task.image.options = {};
config.task.image.options.interlaced = true; // use interlaced PNG and GIF
config.task.image.options.progressive = true; // use progressive JPEG

// ----------------------------------------------------------------------------
// sprite

config.task.sprite = {};

// source
config.task.sprite.source = [];
config.task.sprite.source.push('sprite/**/*.png');

// options
config.task.sprite.options = {};

// template
config.task.sprite.options.template = 'misc/sprite-template.sass';

// ----------------------------------------------------------------------------
// local HTTP server

config.task.serve = {};

// options
config.task.serve.options = {};

// base directory (default: config.directory.preview)
config.task.serve.options.directory = config.directory.preview;

// server host (default: '127.0.0.1')
config.task.serve.options.host = '0.0.0.0';

// server port (default: 8080)
config.task.serve.options.port = 8448;

// use php server (php >= 5.4.0)
config.task.serve.options.php = false; // never use
// config.task.serve.options.php = true; // use default setting
// config.task.serve.options.php = 'misc/router-for-wordpress.php'; // use custom routing script

// php executable
config.task.serve.options.php_exe = [];
config.task.serve.options.php_exe.push('/usr/local/homebrew/bin/php');
config.task.serve.options.php_exe.push('/usr/local/bin/php');
config.task.serve.options.php_exe.push('/usr/bin/php');
config.task.serve.options.php_exe.push('c:/xampp/php/php.exe');

// php.ini
config.task.serve.options.php_ini = [];
config.task.serve.options.php_ini.push('misc/php.win32.ini');
config.task.serve.options.php_ini.push('misc/php.darwin.ini');
config.task.serve.options.php_ini.push('misc/php.ini');

// ============================================================================

module.exports = config;
