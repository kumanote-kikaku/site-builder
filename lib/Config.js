var node_path = require('path').posix;
var gulp_util = require('gulp-util');

function _getSource(config, mapFunc, propName, taskName, ignoreTasks) {
  var task = config.getTaskConfig(taskName);
  var result = [];
  if (!(propName in task)) {
    return result;
  }
  result = task[propName].map(function(pattern) {
    return config[mapFunc](pattern);
  });

  // 除外
  if (Array.isArray(ignoreTasks)) {
    ignoreTasks.forEach(function(taskName) {
      var task = config.getTaskConfig(taskName, {});
      if (propName in task) {
        task[propName].map(function(pattern) {
          result.push('!' + config.getSourcePath(pattern));
        });
      }
    });
  }

  return result;
}

function Config() {
  var name = 'build-config.js';
  var path = node_path.resolve('../../../' + name);
  gulp_util.log('Loading ' + name);
  this._config = require('../../../' + name);
  gulp_util.log('Check ' + name);
  if (typeof this._config.encoding !== 'string') {
    throw new Error('encoding option will must set in ' + name);
  }
  if (typeof this._config.eol !== 'string') {
    throw new Error('eol option will must set in ' + name);
  }
  if (typeof this._config.directory !== 'object') {
    throw new Error('directory option will must set in' + name);
  }
  if (typeof this._config.directory.source !== 'string') {
    throw new Error('directory.source option will must set in' + name);
  }
  if (typeof this._config.directory.cache !== 'string') {
    throw new Error('directory.cache option will must set in' + name);
  }
  if (typeof this._config.directory.preview !== 'string') {
    throw new Error('directory.preview option will must set in' + name);
  }
  if (typeof this._config.directory.release !== 'string') {
    throw new Error('directory.release option will must set in' + name);
  }
  if (typeof this._config.task !== 'object') {
    throw new Error('task option will must set in' + name);
  }
}

Config.prototype.getSourcePath = function(fragment) {
  if (arguments.length === 0) {
    return this._config.directory.source;
  }
  return node_path.join(this._config.directory.source, fragment);
};

Config.prototype.getCachePath = function(fragment) {
  if (arguments.length === 0) {
    return this._config.directory.cache;
  }
  return node_path.join(this._config.directory.cache, fragment);
};

Config.prototype.getPreviewPath = function(fragment) {
  if (arguments.length === 0) {
    return this._config.directory.preview;
  }
  return node_path.join(this._config.directory.preview, fragment);
};

Config.prototype.getReleasePath = function(fragment) {
  if (arguments.length === 0) {
    return this._config.directory.release;
  }
  return node_path.join(this._config.directory.release, fragment);
};

// タスクの設定について繰り返し
Config.prototype.forEachTaskConfig = function(handler) {
  for (var taskName in this._config.task) {
    if (/^[a-z][a-z_-]*$/i.test(taskName)) {
      handler(taskName, this.getTaskConfig(taskName));
    }
  }
};

// livereloadするかどうか
Config.prototype.isLivereloadEnabled = function() {
  return this._config.livereload !== false;
};

// gulp-livereload用の設定取得
Config.prototype.getLivereloadOptions = function() {
  var options = this._config.livereload;
  if (typeof options === 'object' && options !== null) {
    return options;
  }
  return {};
};

Config.prototype.isServerEnabled = function() {
  return this.getTaskConfig('serve', null) !== null;
};

// タスクの設定取得
Config.prototype.getTaskConfig = function(taskName, fallback) {
  if (!(taskName in this._config.task)) {
    if (arguments.length === 1) {
      throw new Error('task ' + taskName + ' not found');
    }
    return fallback;
  }
  return this._config.task[taskName];
};

// タスクのエンコーディング取得
Config.prototype.getEncoding = function(taskName) {
  var task = this.getTaskConfig(taskName);
  return task.encoding || this._config.encoding;
};

// タスクの行末取得
Config.prototype.getEol = function(taskName) {
  var task = this.getTaskConfig(taskName);
  return task.eol || this._config.eol;
};

Config.prototype.getOptions = function(taskName) {
  var task = this.getTaskConfig(taskName);
  return task.options || {};
};

Config.prototype.getOption = function(taskName, name, fallback) {
  var options = this.getOptions(taskName);
  return (name in options) ? options[name] : fallback;
};

Config.prototype.getSourceOfTask = function(taskName, ignoreTasks) {
  return _getSource(this, 'getSourcePath', 'source', taskName, ignoreTasks);
};

Config.prototype.getLibOfTask = function(taskName, ignoreTasks) {
  return _getSource(this, 'getSourcePath', 'library', taskName, ignoreTasks);
};

Config.prototype.getCacheOfTask = function(taskName, ignoreTasks) {
  return _getSource(this, 'getCachePath', 'source', taskName, ignoreTasks);
};

Config.prototype.getPreviewOfTask = function(taskName, ignoreTasks) {
  return _getSource(this, 'getPreviewPath', 'source', taskName, ignoreTasks);
};

Config.prototype.getReleaseOfTask = function(taskName, ignoreTasks) {
  return _getSource(this, 'getReleasePath', 'source', taskName, ignoreTasks);
};

module.exports = new Config();
