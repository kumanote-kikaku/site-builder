// ============================================================================

// ローカルタスク - ファイルコピー

// ============================================================================

var gulp = require('gulp');

var util = require('../util.js');
var config = require('../Config.js');

// ============================================================================

// ローカルタスク名
var NAME = 'copy';

// ソースファイル
var SOURCE = config.getSourceOfTask(NAME);

// ============================================================================

// ビルド処理
function builder(full) {
  // コピーするだけ
  return util.getSourceStream(SOURCE, config.getReleasePath(), full)
    .pipe(gulp.dest(config.getPreviewPath()))
    .pipe(gulp.dest(config.getReleasePath()))
    .pipe(util.logger(NAME, 'green', '[Preview,Release]'));
}

// ============================================================================

module.exports = {
  clean: function(cb) {
    // プレビューとリリースを削除
    util.del([
      config.getPreviewOfTask(NAME),
      config.getReleaseOfTask(NAME)], 'unlink', cb);
  },

  build: function(cb) {
    return builder();
  },

  watch: function(cb) {
    gulp.watch(SOURCE, [NAME]);
    cb();
  }
};

// ============================================================================

// 個別タスク（変更有を対象）
gulp.task(NAME, function(cb) {
  return util.getLivereloadStream(builder());
});

// 個別タスク（全対象）
gulp.task(util.getFullTaskName(NAME), function(cb) {
  return util.getLivereloadStream(builder(true));
});
