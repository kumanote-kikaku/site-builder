// ============================================================================

// ローカルタスク - ファイル整形

// ============================================================================

var gulp = require('gulp');

var include = require('gulp-file-include');
var replace = require('gulp-replace');

var util = require('../util.js');
var config = require('../Config.js');

// ============================================================================

// タスク名
var NAME = 'file';

// ライブラリファイル
var LIB = config.getLibOfTask(NAME);

// ソースファイル
var SOURCE = config.getSourceOfTask(NAME).concat(util.ignoreFiles(LIB));

// 文字コードの置き換えパターン
var ENCODING_PATTERN = /\{\{(ENCODING|CHARSET)\}\}/g;

// ============================================================================

// ビルド処理
function builder(full) {
  var stream;
  var indent = config.getOption(NAME, 'indent', null);
  var prefix = config.getOption(NAME, 'prefix', '@@');
  var suffix = config.getOption(NAME, 'suffix', '');
  var originalIndent = config.getOption(NAME, 'originalIndent', null);
  var indentReg = null;
  function indentReplace(matches) {
    return util.makeString(
      indent.character,
      Math.ceil(matches.length / originalIndent.size) * indent.size);
  }

  if (indent &&
      typeof indent.character === 'string' &&
      isFinite(indent.size) &&
      indent.size > 0 &&
      originalIndent &&
      typeof originalIndent.character === 'string' &&
      isFinite(originalIndent.size) &&
      originalIndent.size > 0) {
    indentReg = new RegExp(
      '^(\\x' +
        ('00' + originalIndent.character.charCodeAt(0).toString(16)).slice(-2) + '+)',
      'mg');
  }

  stream = util.getSourceStream(SOURCE, config.getReleasePath(), full)

  // エンコーディングの置き換え
    .pipe(replace(ENCODING_PATTERN, config.getEncoding(NAME)))

  // ファイル挿し込み
    .pipe(include({
      prefix: prefix,
      suffix: suffix,
      basepath: config.getSourcePath()
    }))

  // 改行コード統一
    .pipe(util.eol(NAME))

  // 文字コード統一
    .pipe(util.convert(NAME));

  // インデント
  if (indentReg) {
    stream = stream
      .pipe(replace(indentReg, indentReplace));
  }

  return stream

  // プレビューに保存
    .pipe(gulp.dest(config.getPreviewPath()))

  // リリースコメント制御
    .pipe(replace(util.RELEASE_PATTERN, ''))

  // リリースに保存
    .pipe(gulp.dest(config.getReleasePath()))

  // ログ
    .pipe(util.logger(NAME, 'magenta', '[Preview,Release]'));
}

// ============================================================================

module.exports = {
  clean: function(cb) {
    // プレビューとリリースを削除
    util.del([
      config.getPreviewOfTask(NAME),
      config.getReleaseOfTask(NAME)], 'unlink', cb);
  },

  // cacheはなし

  build: function(cb) {
    return builder();
  },

  watch: function(cb) {
    gulp.watch(SOURCE, [NAME]);
    gulp.watch(LIB, [util.getFullTaskName(NAME)]);
    cb();
  }
};

// ============================================================================

// 個別タスク（変更有を対象）
gulp.task(NAME, function(cb) {
  return util.getLivereloadStream(builder());
});

// 個別タスク（全対象）
gulp.task(util.getFullTaskName(NAME), function(cb) {
  return util.getLivereloadStream(builder(true));
});
