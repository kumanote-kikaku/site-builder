// ============================================================================

// ローカルタスク - SASS/SCSS

// ============================================================================

var gulp = require('gulp');
var del = require('del');

var sass = require('gulp-sass');
var header = require('gulp-header');
var rename = require('gulp-rename');
var csscomb = require('gulp-csscomb');
var replace = require('gulp-replace');
var minifyCss = require('gulp-minify-css');
var autoprefixer = require('gulp-autoprefixer');

var util = require('../util.js');
var config = require('../Config.js');

// ============================================================================

// ローカルタスク名
var NAME = 'sass';

// ライブラリ
var LIB = config.getLibOfTask(NAME);

// ソース
var SOURCE = config.getSourceOfTask(NAME).concat(util.ignoreFiles(LIB));

// ヘッダのcharset削除パターン
var HEADER_PATTERN = /@charset\s+("[^"]+"|'[^']+')\s*;/g;

// ============================================================================

// ビルド処理
function builder(full) {
  var minify = config.getOption(NAME, 'minify', false);
  var encoding = config.getEncoding(NAME);
  var eolChar = config.getEol(NAME);

  var stream = util.getSourceStream(SOURCE, config.getPreviewPath(), full, {
    // 拡張子が異なるのでオプションで指定
    extension: '.css'
  })

  // SASS/SCSSコンパイル
    .pipe(sass({
      includePaths: [
        config.getSourcePath('css'),
        config.getCachePath('css')
      ]
    }).on('error', sass.logError))

  // vender prefix最適化
    .pipe(autoprefixer({
      browsers: config.getOption(NAME, 'browsers', {})
    }))

  // 整形
    .pipe(csscomb());

  if (minify === true) {
    // 単純なminify
    stream = stream.pipe(minifyCss());
  }

  stream = stream
  // 先頭部分のcharsetを一旦削除
    .pipe(replace(HEADER_PATTERN, ''))

  // 改行コード統一
    .pipe(util.eol(NAME))

  // 文字コード統一
    .pipe(util.convert(NAME))

  // charsetを強制的に設定
    .pipe(header('@charset "' + config.getEncoding(NAME) + '";' + config.getEol(NAME)))

  // プレビューに保存
    .pipe(gulp.dest(config.getPreviewPath()))

  // リリースコメント制御
    .pipe(replace(util.RELEASE_PATTERN, ''))

  // リリースに保存
    .pipe(gulp.dest(config.getReleasePath()))

  // ログ
    .pipe(util.logger(NAME, 'blue', '[Preview,Release]'));

  if (minify === 'both') {
    // .min.cssの処理

    stream = stream

    // 拡張子変更
      .pipe(rename({
        extname: '.min.css'
      }))

    // minify
      .pipe(minifyCss())

    // プレビューとリリースに保存
      .pipe(gulp.dest(config.getPreviewPath()))
      .pipe(gulp.dest(config.getReleasePath()))

    // ログ
      .pipe(util.logger(NAME, 'blue', '[Preview,Release]'));
  }
  return stream;
}

// ============================================================================

module.exports = {
  clean: function(cb) {
    // プレビューとリリースを削除
    util.del([
      config.getPreviewOfTask(NAME),
      config.getReleaseOfTask(NAME)], 'unlink', cb);
  },

  build: function(cb) {
    return builder();
  },

  watch: function(cb) {
    gulp.watch(SOURCE, [NAME]);
    gulp.watch(LIB, [util.getFullTaskName(NAME)]);
    // spriteのscssも監視
    gulp.watch(config.getCachePath('css/_sprite.sass'), [util.getFullTaskName(NAME)]);
    cb();
  }
};

// ============================================================================

// 個別タスク（変更有を対象）
gulp.task(NAME, function(cb) {
  return util.getLivereloadStream(builder(false));
});

// 個別タスク（全対象）
gulp.task(util.getFullTaskName(NAME), function(cb) {
  return util.getLivereloadStream(builder(true));
});
