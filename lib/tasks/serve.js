// ============================================================================

// ローカルタスク - Webサーバ

// ============================================================================

var gulp = require('gulp');
var connect = require('gulp-connect-php');
var webserver = require('gulp-webserver');

var config = require('../Config.js');
var util = require('../util.js');

var node_fs = require('fs');
var node_path = require('path');

// ============================================================================

// タスク名
var NAME = 'serve';

// ソース
var SOURCE = config.getOption(NAME, 'directory', config.getPreviewPath());

// ============================================================================

// phpの実行ファイルを探す
function findPhpExe(list) {
  if (!Array.isArray(list)) {
    return null;
  }
  for (var path, l = list.length, i = 0; i < l; ++i) {
    path = list[i];
    if (node_fs.existsSync(path)) {
      return node_path.resolve(path);
    }
  }
  return null;
}

// php.iniを探す
function findPhpIni(list) {
  if (!Array.isArray(list)) {
    return null;
  }

  var platform = process.platform;

  // platformが入っているものを優先
  for (var path, l = list.length, i = 0; i < l; ++i) {
    path = list[i];
    if (path.indexOf(platform) < 0) continue;
    if (node_fs.existsSync(path)) {
      return node_path.resolve(path);
    }
  }
  for (i = 0; i < l; ++i) {
    path = list[i];
    if (path.indexOf(platform) >= 0) continue;
    if (node_fs.existsSync(path)) {
      return node_path.resolve(path);
    }
  }
  return null;
}

// gulp-webserverはそのまま使うとcharset=UTF-8になるので
// middlewareで制御する
function middleware(req, res, next) {
  var type = null;
  if (/^\.html$/.test(req.url) ||
      /\/$/.test(req.url)) {
    type = 'text/html';
  } else if (/^\.css$/.test(req.url)) {
    type = 'text/css';
  } else if (/^\.js$/.test(req.url)) {
    type = 'text/javascript';
  }
  if (type !== null) {
    res.setHeader('Content-Type', type + ';charset=' + config.getEncoding(NAME));
  }
  next();
}

// gulp-webserverでサーバを立てる
function useJs(host, port) {
  return gulp.src(SOURCE)
    .pipe(webserver({
      host: host,
      port: port,
      middleware: middleware
    }))
    .pipe(util.notifier('start node server'));
}

function usePhp(host, port, php, cb) {
  var options = {
    base: SOURCE,
    hostname: host,
    port: port
  };
  var exe = findPhpExe(config.getOption(NAME, 'php_exe', []));
  var ini = findPhpIni(config.getOption(NAME, 'php_ini', []));
  if (exe !== null) {
    options.bin = exe;
  }
  if (ini !== null) {
    options.ini = ini;
  }
  if (typeof php === 'string' && node_fs.existsSync(php)) {
    options.router = node_path.resolve(php);
  }
  connect.server(options, function() {
    util.notify('start php cli server');
    cb();
  });
}

// ============================================================================

// 個別タスク
gulp.task('serve', ['watch'], function(cb) {
  var host = config.getOption(NAME, 'host', '127.0.0.1');
  var port = config.getOption(NAME, 'port', 8080);
  var php = config.getOption(NAME, 'php', false);

  if (php === false) {
    useJs(host, port);
  } else {
    usePhp(host, port, php, cb);
  }
});
