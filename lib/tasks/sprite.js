// ============================================================================

// ローカルタスク - CSSスプライト

// ============================================================================

var gulp = require('gulp');
var del = require('del');
var globby = require('globby');
var imagemin = require('gulp-imagemin');
var spritesmith = require('gulp.spritesmith');

var util = require('../util.js');
var config = require('../Config.js');

var node_fs = require('fs');

// ============================================================================

// タスク名
var NAME = 'sprite';

// キャッシュ用のタスク名
var CACHE_NAME = NAME + '-cache';

// ソース
var SOURCE = config.getSourceOfTask(NAME);

// キャッシュ
var CACHE = config.getCachePath(util.SPRITE_PATTERN);

// テンプレート
var TEMPLATE = config.getOption(NAME, 'template', 'misc/sprite-template.sass');

// ============================================================================

// 最新のCSSスプライト画像ファイルを探す
function getCurrentSpriteFile() {
  var files = globby.sync(config.getCachePath(util.SPRITE_PATTERN));
  return files.length > 0 ? files[files.length - 1] : null;
}

// キャッシュ処理
function cacher(full, cb) {
  var stream;

  // 一番新しいspriteファイルの日付を取得する
  var file = getCurrentSpriteFile();
  var spriteStamp = (file === null ? 0 : node_fs.statSync(file).mtime.valueOf());

  // ソースのファイルの日付のうち、最新のもの取得する
  var sourceStamp = globby.sync(SOURCE).reduce(function(current, path) {
    return Math.max(current, node_fs.statSync(path).mtime.valueOf());
  }, 0);

  if (full || spriteStamp < sourceStamp) {
    // 新しいファイルがある時だけキャッシュを作る
    streams = util.getSourceStream(SOURCE, config.getCachePath(), true)

    // スプライト生成
      .pipe(spritesmith({
        imgName: '../images/sprite.' + Date.now() + '.png',
        cssName: '_sprite.sass',
        cssFormat: 'sass',
        algorithm: 'binary-tree',
        padding: 20,
        // sassのテンプレート
        cssTemplate: TEMPLATE
      }));

    util.parallel([function() {
      return streams.img

      // 画像最適化
        .pipe(imagemin())

      // キャッシュに保存
        .pipe(gulp.dest(config.getCachePath('css')))

      // ログ
        .pipe(util.logger(NAME, 'green'));

    }, function() {
      return streams.css

      // キャッシュに保存
        .pipe(gulp.dest(config.getCachePath('css')))

      // ログ
        .pipe(util.logger(NAME, 'green'));

    }], cb);

    return;
  }

  // 新しいファイルがなければなにもしない
  cb();
}

// ビルド処理
function builder(cb) {
  // 一番新しいものをコピーする
  var file = getCurrentSpriteFile();
  if (file === null) {
    // ファイルがない場合は無視
    cb();
    return;
  }

  return gulp.src([file])

  // プレビューとリリースに保存
    .pipe(gulp.dest(config.getPreviewPath('images')))
    .pipe(gulp.dest(config.getReleasePath('images')))

  // ログ
    .pipe(util.logger(NAME, 'green', '[Preview,Release]'));
}

// ============================================================================

module.exports = {
  clean: function(cb) {
    // プレビューとリリースを削除
    del([
      config.getPreviewPath(util.SPRITE_PATTERN),
      config.getReleasePath(util.SPRITE_PATTERN)], function(err, deleteFiles) {
        util.cleanLog(deleteFiles, 'unlink');

        // 一番新しいキャッシュ以外のみ削除
        var files = globby.sync(config.getCachePath(util.SPRITE_PATTERN));

        if (files.length < 2) {
          cb();
          return;
        }

        del(files.slice(0, -1), function(err, deleteFiles) {
          util.cleanLog(deleteFiles, 'unlink');
          cb();
        });
      });
  },

  cache: function(cb) {
    cacher(false, cb);
  },

  build: function(cb) {
    return builder(cb);
  },

  watch: function(cb) {
    gulp.watch(SOURCE, [NAME]);
    gulp.watch([TEMPLATE], [util.getFullTaskName(NAME)]);
    cb();
  }
};

// ============================================================================


// 個別タスク（キャッシュを作成）
gulp.task(CACHE_NAME, function(cb) {
  return cacher(false, cb);
});

// 個別タスク（強制的にキャッシュを作成）
gulp.task(util.getFullTaskName(CACHE_NAME), function(cb) {
  return cacher(true, cb);
});

// 個別タスク（キャッシュをコピー）
gulp.task(NAME, [CACHE_NAME], function(cb) {
  return builder(cb);
});

// 個別タスク（強制作成＋キャッシュをコピー）
gulp.task(util.getFullTaskName(NAME), [util.getFullTaskName(CACHE_NAME)], function(cb) {
  return builder(cb);
});
