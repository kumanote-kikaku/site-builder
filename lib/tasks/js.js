// ============================================================================

// ローカルタスク - JavaScript

// ============================================================================

var gulp = require('gulp');
var del = require('del');

var rename = require('gulp-rename');
var uglify = require('gulp-uglify');
var replace = require('gulp-replace');
var jsPrettify = require('gulp-js-prettify');
var stripComments = require('gulp-strip-comments');

var util = require('../util.js');
var config = require('../Config.js');

// ============================================================================

// ローカルタスク名
var NAME = 'js';

// ソース
var SOURCE = config.getSourceOfTask(NAME, ['copy']);

// ============================================================================

// ビルド処理
function builder(full) {
  var strip = config.getOption(NAME, 'strip', false);
  var indent = config.getOption(NAME, 'indent', null);
  var minify = config.getOption(NAME, 'minify', false);
  var cleanupOptions = {
    indent_size: 2,
    indent_char: ' ',
    indent_level: 0,
    indent_with_tabs: false,
    preserve_newlines: false
  };

  var stream = util.getSourceStream(SOURCE, config.getReleasePath(), full);

  if (strip) {
    stream = stream
    // ブロックコメント削除
      .pipe(stripComments({
        line: true
      }));
  }

  if (indent &&
      typeof indent.character === 'string' &&
      isFinite(indent.size) &&
      indent.size > 0) {
    stream = stream.
      // インデント
      pipe(jsPrettify({
        indent_size: indent.size,
        indent_char: indent.character,
        indent_level: 0,
        indent_with_tabs: false,
        preserve_newlines: false
      }));
  }

  stream = stream

  // 改行コード統一
    .pipe(util.eol(NAME))

  // 文字コード統一
    .pipe(util.convert(NAME));

  if (minify === true) {
    // 単純なminify
    stream = stream.pipe(uglify());
  }

  stream = stream

  // プレビューに保存
    .pipe(gulp.dest(config.getPreviewPath()))

  // リリースコメント制御
    .pipe(replace(util.RELEASE_PATTERN, ''))

  // リリースに保存
    .pipe(gulp.dest(config.getReleasePath()))

  // ログ
    .pipe(util.logger(NAME, 'cyan', '[Preview,Release]'));

  if (minify === 'both') {
    // .min.cssの処理

    stream = stream

    // 拡張子変更
      .pipe(rename({
        extname: '.min.js'
      }))

    // minify
      .pipe(uglify())

    // プレビューとリリースに保存
      .pipe(gulp.dest(config.getPreviewPath()))
      .pipe(gulp.dest(config.getReleasePath()))

    // ログ
      .pipe(util.logger(NAME, 'cyan', '[Preview,Release]'));
  }

  return stream;
}

// ============================================================================

module.exports = {
  clean: function(cb) {
    // プレビューとリリースを削除
    util.del([
      config.getPreviewOfTask(NAME),
      config.getReleaseOfTask(NAME)], 'unlink', cb);
  },

  build: function(cb) {
    return builder();
  },

  watch: function(cb) {
    gulp.watch(SOURCE, [NAME]);
    cb();
  }
};

// ============================================================================

// 個別タスク（変更有を対象）
gulp.task(NAME, function(cb) {
  return util.getLivereloadStream(builder());
});

// 個別タスク（全対象）
gulp.task(util.getFullTaskName(NAME), function(cb) {
  return util.getLivereloadStream(builder(true));
});
