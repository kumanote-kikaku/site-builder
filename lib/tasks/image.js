// ============================================================================

// ローカルタスク - 画像最適化

// ============================================================================

var gulp = require('gulp');
var globby = require('globby');
var imagemin = require('gulp-imagemin');

var util = require('../util.js');
var config = require('../Config.js');

// ============================================================================

// タスク名
var NAME = 'image';

// キャッシュ用のタスク名
var CACHE_NAME = NAME + '-cache';

// ソース
var SOURCE = config.getSourceOfTask(NAME, ['sprite']); // spriteは除外

// キャッシュ
var CACHE = config.getCacheOfTask(NAME);

// ============================================================================

// キャッシュ処理
function cacher() {
  return util.getSourceStream(SOURCE, config.getCachePath(), false)

  // 画像最適化
    .pipe(imagemin(config.getOptions(NAME)))

  // キャッシュに保存
    .pipe(gulp.dest(config.getCachePath()))

  // ログ
    .pipe(util.logger(NAME, 'yellow'));
}

// ビルド処理
function builder(full) {
  return util.getSourceStream(CACHE, config.getReleasePath(), full)

  // プレビューとリリースに保存
    .pipe(gulp.dest(config.getPreviewPath()))
    .pipe(gulp.dest(config.getReleasePath()))

  // ログ
    .pipe(util.logger(NAME, 'green', '[Preview,Release]'));
}

// ============================================================================

module.exports = {
  clean: function(cb) {
    // プレビューとリリースを削除
    util.del([
      config.getPreviewOfTask(NAME),
      config.getReleaseOfTask(NAME)], 'unlink', function() {

        // キャッシュにあるけどソースにないものはキャッシュを削除

        var src = [];
        var sprites = [];
        var garbages = [];

        // ソースを収集
        globby.sync(SOURCE).reduce(function(length, path) {
          src.push(path.slice(length));
          return length;
        }, config.getSourcePath().length);

        // CSSスプライトの画像を収集
        sprites = globby.sync(config.getCachePath(util.SPRITE_PATTERN));

        // キャッシュを探す
        globby.sync(config.getCacheOfTask(NAME)).reduce(function(length, path) {
          // ソースになく、CSSスプライトでもない場合に削除
          if (src.indexOf(path.slice(length)) < 0 && sprites.indexOf(path) < 0) {
            garbages.push(path);
          }
          return length;
        }, config.getCachePath().length);

        // 削除
        util.del(garbages, 'unlink', cb);
      });
  },

  cache: function(cb) {
    return cacher();
  },

  build: function(cb) {
    return builder();
  },

  watch: function(cb) {
    gulp.watch(SOURCE, [NAME]);
    cb();
  }
};

// ============================================================================

// 個別タスク（キャッシュを作成）
gulp.task(CACHE_NAME, function(cb) {
  return cacher();
});

// 個別タスク（変更有のみ）
gulp.task(NAME, [CACHE_NAME], function(cb) {
  return util.getLivereloadStream(builder(false));
});

// 個別タスク（全対象）
gulp.task(util.getFullTaskName(NAME), [CACHE_NAME], function(cb) {
  return util.getLivereloadStream(builder(true));
});
