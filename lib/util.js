// ============================================================================

// 汎用ユーティリティ

// ============================================================================

var config = require('./Config.js');

var gulp = require('gulp');
var gulp_util = require('gulp-util');

var del = require('del');
var eol = require('gulp-eol');
var tap = require('gulp-tap');
var chalk = require('chalk');
var globby = require('globby');
var changed = require('gulp-changed');
var convert = require('gulp-convert-encoding');
var replace = require('gulp-replace');
var plumber = require('gulp-plumber');
var livereload = require('gulp-livereload');

var notifier = require('node-notifier');
var flatten = require('lodash.flatten');

var node_fs = require('fs');
var node_path = require('path');
var EventEmitter = require('events').EventEmitter;

var CLEAN_FILES = [
  '.DS_Store',
  'Thumbs.db'
];

// ============================================================================

// 任意の長さの繰り返し文字列を生成する
function makeString(str, length) {
  if (str.length < length) {
    return makeString(str + str, length);
  }
  return str.slice(0, length);
}

// パスの先頭を置き換え
function pathReplace1(path, prefix, alias) {
  if (path.indexOf(prefix) === 0) {
    return alias + path.slice(prefix.length);
  }
  return path;
}

// パスの先頭を識別子に置き換え
function pathReplace(path, dirname) {
  path = pathReplace1(path, config.getSourcePath(), '[Src]');
  path = pathReplace1(path, config.getCachePath(), '[Cache]');
  path = pathReplace1(path, config.getPreviewPath(), '[Preview]');
  path = pathReplace1(path, config.getReleasePath(), '[Release]');
  if (typeof dirname === 'string') {
    path = path.replace(/^\[[A-Z][a-z]+\]/, dirname);
  }
  return path;
}

// ============================================================================

// チェック
function check(cb) {
  // TODO more information for check task.

  gulp_util.log('Checking [' + chalk.green('OK') + ']');

  cb();
}

// ファイルログの出力
function log(title, color, filePath, dirname) {
  var path;
  var pad = makeString(' ', 8 - title.length);
  if (chalk[color]) {
    title = chalk.bold(chalk[color](title));
  }
  path = node_path.relative('.', filePath);
  if (node_path === node_path.win32) {
    path = path.replace(/\\/g, '/');
  }
  console.log('           ' + title + pad + ' : ' + pathReplace(path, dirname));
}

function notify(message) {
  var title = 'site-builder';

  notifier.notify({
    title: title,
    message: message
  });

  console.log('');
  console.log(chalk.bold(chalk.red(title)));
  console.log(chalk.red(message));
  console.log('');
}

// delのラッパー
function delWrapper(files, logTitle, callback) {
  del(flatten(files, true), function(err, deletedFiles) {
    cleanLog(deletedFiles, logTitle);
    callback();
  });
}

// 複数のタスクを実行し、すべて終わってからコールバックする
function parallel(taskArray, cb) {
  var end = [];
  var callbacked = false;

  function onEnd(err, i) {
    // コールバック済みなら何もしない
    if (callbacked) return;

    // エラーがあった場合はすぐにコールバック
    if (err) {
      callbacked = true;
      cb(err);
    }

    // フラグ立てる
    end[i] = true;
    // フラグが立ってる === 処理済みのタスクを集める
    var ended = end.filter(function(x) { return x !== false; });

    // すべて終わっていたらコールバック
    if (end.length === ended.length) {
      callbacked = true;
      cb();
    }
  }

  // Array以外やArrayが空の場合は即コールバック
  if (!Array.isArray(taskArray) || taskArray.length === 0) {
    cb();
    return;
  }

  // フラグリストを作成
  end = taskArray.map(function() { return false; });

  // タスクを順次実行
  taskArray.forEach(function(task, i) {
    var isEventEmitter = false;
    var stream = task(function() {
      // EventEmitterでなければここでリスナーをたたく
      if (!isEventEmitter) {
        onEnd(null, i);
      }
    });
    // EventEmitterであればendイベントを紐付ける
    if ((isEventEmitter = stream instanceof EventEmitter)) {
      stream.on('end', function(err) {
        onEnd(err, i);
      });
    }
  });
}

// 設定を元にタスクを生成する
function makeTasks() {
  var clean = [function(cb) {
    delWrapper([
      config.getCachePath('**/._'),
      config.getCachePath('**/.DS_Store'),
      config.getCachePath('**/Thumbs.db'),
      config.getCachePath('**/Desktop.ini'),
      config.getPreviewPath('**/._'),
      config.getPreviewPath('**/.DS_Store'),
      config.getPreviewPath('**/Thumbs.db'),
      config.getPreviewPath('**/Desktop.ini'),
      config.getReleasePath('**/._'),
      config.getReleasePath('**/.DS_Store'),
      config.getReleasePath('**/Thumbs.db'),
      config.getReleasePath('**/Desktop.ini')], 'unlink', cb);
  }];
  var cache = [];
  var build = [];
  var watch = [];

  gulp_util.log('Loading tasks ...');
  config.forEachTaskConfig(function(taskName, taskConfig) {
    var path = './tasks/' + taskName + '.js';
    var task = require(path);
    log('task', 'white', path);
    if ('clean' in task) clean.push(task.clean);
    if ('cache' in task) cache.push(task.cache);
    if ('build' in task) build.push(task.build);
    if ('watch' in task) watch.push(task.watch);
  });
  gulp_util.log('Loading tasks complete');

  return {
    clean: function(cb) { parallel(clean, cb); },
    cache: function(cb) { parallel(cache, cb); },
    build: function(cb) { parallel(build, cb); },
    watch: function(cb) { parallel(watch, cb); }
  };
}

// 空になるディレクトリを削除する
function cleanDirs(dirPath, callback) {
  var dirs = globby.sync(node_path.join(dirPath, '**', '/'));
  var deleteDirs = [];

  // ゴミファイルを削除
  dirs.forEach(function(dirPath) {
    var deleteFiles = globby.sync(CLEAN_FILES.map(function(name) {
      return node_path.join(dirPath, name);
    })).map(function(path) {
      node_fs.unlinkSync(path);
      return path;
    });
    cleanLog(deleteFiles, 'unlink');
  });

  // ディレクトリの深さの逆順でソート
  dirs.sort(function(a, b) {
    a = a.split('/').length;
    b = b.split('/').length;
    return a > b ? -1 : (a < b ? 1 : 0);
  });

  dirs.forEach(function(dirPath) {
    // 空のディレクトリを削除
    if (globby.sync([
      node_path.join(dirPath, '*'),
      node_path.join(dirPath, '.*')]).length === 0) {
      deleteDirs.push(dirPath);
      node_fs.rmdirSync(dirPath);
    }
  });
  cleanLog(deleteDirs, 'rmdir');
  callback();
}

// ファイル削除のログ
function cleanLog(paths, title) {
  if (Array.isArray(paths)) {
    paths.forEach(function(x) {
      log(title, 'red', x);
    });
  }
}

// ============================================================================

module.exports = {
  check: check,

  makeString: makeString,
  parallel: parallel,
  makeTasks: makeTasks,
  log: log,

  // 全対象タスクの名前を返す
  getFullTaskName: function(name) {
    return name + '!';
  },

  // デフォルトタスク名を返す
  getDefaultTaskName: function() {
    // serveの有効ならserve、それ以外はwatch
    return (config.getTaskConfig('serve', null) === null) ? 'watch' : 'serve';
  },

  // ソース用のストリームを生成する
  getSourceStream: function(source, path, full, options) {
    var stream = gulp.src(source).pipe(plumber({
      errorHandler: function(err) {
        notify('error in `' + err.plugin + '\'\n' + err.message);
      }
    }));
    if (!full) {
      // 変更有のみを抽出
      stream = stream.pipe(changed(path, options || {}));
    }
    return stream;
  },

  // livereload付のstreamにする
  getLivereloadStream: function(stream) {
    if (config.isLivereloadEnabled()) {
      stream = stream
        .pipe(livereload());
    }
    return stream;
  },

  // livereloadのlistenラッパー
  startLivereload: function() {
    if (config.isLivereloadEnabled()) {
      livereload.listen(config.getLivereloadOptions());
    }
  },

  // キャッシュ・プレビュー・リリースの空ディレクトリ削除
  cleanDirs: function(cb) {
    cleanDirs(config.getCachePath(), function() {
      cleanDirs(config.getPreviewPath(), function() {
        cleanDirs(config.getReleasePath(), cb);
      });
    });
  },

  // 無視ファイルパターンの生成
  ignoreFiles: function(files) {
    return files.map(function(x) { return '!' + x; });
  },


  // ファイルログを表示するタスク
  logger: function(title, color, dirname) {
    return tap(function(file, t) {
      log(title, color, file.path, dirname);
    });
  },

  // 削除ログ
  cleanLog: cleanLog,

  // 通知メッセージ
  notify: notify,

  // 通知タスク
  notifier: function(message) {
    return tap(function() {
      notify(message);
    });
  },

  // delのラッパー
  del: delWrapper,

  // gulp-eolのラッパー
  eol: function(taskName) {
    return eol(config.getEol(taskName));
  },

  // gulp-convert-encodingのラッパー
  convert: function(taskName) {
    return convert({
      to: config.getEncoding(taskName)
    });
  },

  // リリース時のコメント制御用正規表現
  RELEASE_PATTERN:/((\/\*|<!--)\s*PUBLIC\.\.\.|\.\.\.PUBLIC\s*(\*\/|-->)|\/\*\s*DEBUG\.\.\.(.|\x0a|\x0d)*?\.\.\.DEBUG\s*\*\/|<!--\s*DEBUG\.\.\.(.|\x0a|\x0d)*?\.\.\.DEBUG\s*-->)/mg,

  // CSSスプライトの画像パターン
  SPRITE_PATTERN: 'images/sprite.*.png'
};
